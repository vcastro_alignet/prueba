<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $incrementing = false;

    protected $connection = 'sqlsrv';

    protected $primaryKey = 'id';

    protected $table = 'categoriesView';

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
}
