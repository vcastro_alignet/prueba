<?php

namespace Alobd;

use Alobd\Traits\GenUuid;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use GenUuid;

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = [
        'user_id', 'platform', 'token'
    ];
}
