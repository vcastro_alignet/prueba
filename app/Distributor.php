<?php

namespace Alobd;

use Alobd\Traits\Attributes\DistributorAttributes;
use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    use DistributorAttributes;

    public $incrementing = false;

    protected $connection = 'sqlsrv_dist';

    protected $primaryKey = 'id';

    protected $table = 'Distributors';

    public function categories()
    {
        return $this->hasMany(Category::class, 'distributor_id');
    }
}
