<?php

namespace Alobd\GraphQL\Fields;

use Folklore\GraphQL\Support\Field;
use GraphQL\Type\Definition\Type;


class LanguageField extends Field
{

    protected $attributes = [
        'description' => "Idiomas disponibles"
    ];

    public function type(){
        return Type::string();
    }

    public function args()
    {
        return [
            'es' => [
                'type' => Type::string(),
                'description' => 'para idioma Español'
            ],
            'en' => [
                'type' => Type::string(),
                'description' => 'for English language'
            ]
        ];
    }

    protected function resolve($root, $args)
    {
        $width = isset($args['es']) ? $args['es']: "es_es_es";
        $height = isset($args['en']) ? $args['en']: "en_en_en";
        return 'http://placehold.it/'.$width.'x'.$height;
    }
}