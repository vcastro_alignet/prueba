<?php

namespace Alobd\GraphQL\Mutation\Password;

use Alobd\Device;
use GraphQL as CoreGraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\Auth;

class DeviceAddMutation extends Mutation
{
    protected $attributes = [
        'name' => 'deviceAdd'
    ];

    public function type()
    {
        return CoreGraphQL::type('Device');
    }

    public function args()
    {
        return [
            'platform' => ['type' => Type::nonNull(Type::string())],
            'token' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args)
    {
        $user = Auth::guard('api')->user();

        return Device::create([
            'user_id' => $user->id,
            'platform' => $args['platform'],
            'token' => $args['token'],
        ]);
    }
}