<?php

namespace Alobd\GraphQL\Mutation\Password;

use Alobd\Order;
use GraphQL;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\Auth;

class OrderAddMutation extends Mutation
{
    protected $attributes = [
        'name' => 'orderAdd'
    ];

    public function type()
    {
        return GraphQL::type('Order');
    }

    public function args()
    {
        return [
            'distributor_id' => ['type' => Type::nonNull(Type::string())],
            'payment_id' => ['type' => Type::nonNull(Type::int())],
            'user_id' => ['type' => Type::nonNull(Type::string())],
            'store_id' => ['type' => Type::nonNull(Type::string())],
            'details' => [
                'name' => 'details',
                'type' => Type::listOf(
                    new InputObjectType([
                        'name' => 'OrderDetails',
                        'fields' => [
                            'product_id' => [
                                'name' => 'product_id',
                                'type' => Type::nonNull(Type::string())
                            ],
                            'quantity' => [
                                'name' => 'quantity',
                                'type' => Type::nonNull(Type::int())
                            ],
                            'note' => [
                                'name' => 'note',
                                'type' => Type::string()
                            ],
                            'added_at' => [
                                'name' => 'added_at',
                                'type' => Type::nonNull(Type::string())
                            ]
                        ]
                    ])
                )
            ]
        ];
    }

    public function resolve($root, $args)
    {
        //$user = Auth::guard('api')->user();

        return Order::first();
    }
}