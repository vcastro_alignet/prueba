<?php

namespace Alobd\GraphQL\Mutation;

use Alobd\User;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use GuzzleHttp\Client as GhClient;
use GuzzleHttp\Exception\GuzzleException;
use Laravel\Passport\Client as PassportClient;

class UserLoginMutation extends Mutation
{
    protected $attributes = [
        'name' => 'userLoginMutation'
    ];

    public function type()
    {
        return GraphQL::type('Passport');
    }

    public function args()
    {
        return [
            'mobile' => ['type' => Type::nonNull(Type::string())],
            'password' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args)
    {
        $http = new GhClient;
        $passport = PassportClient::where('password_client', 1)->first();

        $response = $http->post(route('oauth.token'), [
            'form_params' => [
                'grant_type'    => 'password',
                'client_id'     => $passport->id,
                'client_secret' => $passport->secret,
                'username'      => $args['mobile'],
                'password'      => $args['password'],
                'scope'         => null,
            ]
        ]);

        return json_decode($response->getBody(), true);

    }
}