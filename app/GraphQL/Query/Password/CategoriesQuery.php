<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Category;
use Alobd\User;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;


class CategoriesQuery extends Query
{
    protected $attributes = [
        'name' => 'categories'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Category'));
    }

    public function args()
    {
        return [
            'distributor_id' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args)
    {
        return Category::where('distributor_id', $args['distributor_id'])->get();
    }
}