<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Distributor;
use Alobd\User;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;
use Illuminate\Support\Facades\Auth;


class DistributorsQuery extends Query
{
    protected $attributes = [
        'name' => 'distributors',
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Distributor'));
    }

    public function args()
    {
        return [
            'store_id' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        $user = Auth::guard('api')->user();

        foreach (\DB::select('exec dbo.GetDistributorsAvailable @StoreId=?', [$user->store_id]) as $store_id)
            $res[] = \Alobd\Distributor::find($store_id->Id);

        return $res;
    }
}
