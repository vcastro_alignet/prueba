<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\User;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;
use Illuminate\Support\Facades\Auth;


class MeQuery extends Query
{
    protected $attributes = [
        'name' => 'me',
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return;
    }

    public function resolve()
    {
        return Auth::guard('api')->user();

    }
}
