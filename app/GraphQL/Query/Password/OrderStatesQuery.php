<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Order;
use Alobd\OrderState;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;


class OrderStatesQuery extends Query
{
    protected $attributes = [
        'name' => 'orderStates',
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('OrderState'));
    }

    public function args()
    {
        return ;
    }

    public function resolve($root, $args)
    {
        return OrderState::all();
    }
}