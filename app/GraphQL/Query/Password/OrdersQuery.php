<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Order;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;


class OrdersQuery extends Query
{
    protected $attributes = [
        'name' => 'orders',
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Order'));
    }

    public function args()
    {
        return [
            'order_state' => ['type' => Type::string()],
            'payment_state' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['order_state']))
            return Order::where('order_state_id', $args['order_state'])->get();

        if (isset($args['payment_state']))
            return Order::where('order_state_id', $args['payment_state'])->get();

        return Order::all();
    }
}