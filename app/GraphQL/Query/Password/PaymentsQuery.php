<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Order;
use Alobd\Payment;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;


class PaymentsQuery extends Query
{
    protected $attributes = [
        'name' => 'payments',
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Payment'));
    }

    public function args()
    {
        return ;
    }

    public function resolve($root, $args)
    {
       return Payment::all();
    }
}