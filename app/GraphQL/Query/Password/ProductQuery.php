<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Product;
use Alobd\User;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;
use Illuminate\Support\Str;


class ProductQuery extends Query
{
    protected $attributes = [
        'name' => 'product',
    ];

    public function type()
    {
        return GraphQL::type('Product');
    }

    public function args()
    {
        return [
            'id' => ['type' => Type::string()],
            'ean' => ['type' => Type::string()],
            'distributor_id' => ['type' => Type::nonNull(Type::string()), 'description' => 'Se requiere validar si se encuentra disponible el distribuidor'],
        ];
    }

    public function resolve($root, $args)
    {
        $prods = Product::where('distributor_id', $args['distributor_id']);

        if (isset($args['id']))
            $prods->where('id', $args['id']);

        if (isset($args['ean']))
            $prods->where('ean', $args['ean']);

        return $prods->first();
    }
}