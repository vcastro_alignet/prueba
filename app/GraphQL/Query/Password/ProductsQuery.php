<?php

namespace Alobd\GraphQL\Query\Password;

use Alobd\Product;
use Alobd\User;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;
use Illuminate\Support\Str;


class ProductsQuery extends Query
{
    protected $attributes = [
        'name' => 'products',
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Product'));
    }

    public function args()
    {
        return [
            'distributor_id' => ['type' => Type::nonNull(Type::string())],
            'category_id' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        $prods = Product::where('distributor_id', $args['distributor_id']);

        if(isset($args['category_id']))
            $prods->where('category_id', $args['category_id']);

        return $prods->get();
    }
}