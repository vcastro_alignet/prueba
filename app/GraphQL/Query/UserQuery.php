<?php

namespace Alobd\GraphQL\Query;

use Alobd\User;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;


class UserQuery extends Query
{
    protected $attributes = [
        'name' => 'user',
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'id' => ['type' => Type::string()],
            'mobile' => ['type' => Type::string()],
            'email' => ['type' => Type::string()],
            'dni' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id']))
            return User::find($args['id']);

        if (isset($args['mobile']))
            return User::where('mobile', $args['mobile'])->first();

        if (isset($args['email']))
            return User::where('email', $args['email'])->first();

        if (isset($args['dni']))
            return User::where('dni', $args['dni'])->first();

        return null;
    }
}