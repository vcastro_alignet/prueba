<?php

namespace Alobd\GraphQL\Query;

use Alobd\User;

use Alobd\Version;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Folklore\GraphQL\Support\Facades\GraphQL;


class VersionQuery extends Query
{
    protected $attributes = [
        'name' => 'version',
    ];

    public function type()
    {
        return GraphQL::type('Version');
    }

    public function args()
    {
        return [
            'product_code' => ['type' => Type::nonNull(Type::string())],
            'platform' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['product_code']) && isset($args['platform']))
            return Version::where('product_code', $args['product_code'])->where('platform', $args['platform'])->first();

        return null;
    }
}