<?php

namespace Alobd\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class CategoryType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Category',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'distributor_id' => ['type' => Type::string()],
            'products' => ['type' => Type::listOf(GraphQL::type('Product'))],
        ];
    }
}