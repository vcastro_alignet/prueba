<?php

namespace Alobd\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class DeviceType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Device',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'user_id' => ['type' => Type::string()],
            'platform' => ['type' => Type::string()],
            'token' => ['type' => Type::string()],
            'ip_address' => ['type' => Type::string()],
        ];
    }
}