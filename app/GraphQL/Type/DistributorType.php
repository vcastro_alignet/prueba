<?php

namespace Alobd\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class DistributorType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Distributor',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'company' => ['type' => Type::string()],
            'ruc' => ['type' => Type::string()],
            'address' => ['type' => Type::string()],
            'image' => ['type' => Type::string()],
            'color' => ['type' => Type::string()],
            'min_order' => ['type' => Type::float()],
            'categories' => [
                'args' => [
                    'id' => ['type' => Type::string()],
                ],
                'type' => Type::listOf(GraphQL::type('Category')),
            ],
        ];
    }

    public function resolveCategoriesField($root, $args)
    {
        if (isset($args['id']))
            return  $root->categories->where('id', $args['id']);

        return $root->categories;
    }
}