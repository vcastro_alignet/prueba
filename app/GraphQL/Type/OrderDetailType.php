<?php

namespace Alobd\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Facades\GraphQL;
use Folklore\GraphQL\Support\Type as GraphQLType;

class OrderDetailType extends GraphQLType
{
    protected $attributes = [
        'name' => 'OrderDetail',
    ];

    public function fields()
    {
        return [
            'product' => ['type' => GraphQL::type('Product')],
            'quantity' => ['type' => Type::int()],
            'note' => ['type' => Type::string()],
            'added_at' => ['type' => Type::string()],
        ];
    }
}