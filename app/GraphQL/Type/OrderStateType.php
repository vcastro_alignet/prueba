<?php

namespace Alobd\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class OrderStateType extends GraphQLType
{
    protected $attributes = [
        'name' => 'OrderState',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => [
                'args' => [
                    'lang' => ['type' => Type::string()]
                ],
                'type' => Type::string(),
                'resolve' => function($root, $args) {
                    $lang = isset($args['lang']) ? $args['lang'] : 'es';
                    return $root->lang->where('lang_id', $lang)->first()->name;
                }
            ],
            'color' => ['type' => Type::string()],
        ];
    }

}