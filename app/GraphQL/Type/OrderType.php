<?php

namespace Alobd\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;

use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;

class OrderType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Order',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())],
            'distributor' => ['type' => GraphQL::type('Distributor')],
            'payment' => ['type' => GraphQL::type('Payment')],
            'payment_state' => ['type' => GraphQL::type('PaymentState')],
            'order_state' => ['type' => GraphQL::type('OrderState')],
            'user' => ['type' => GraphQL::type('User')],
            'store' => ['type' => GraphQL::type('Store')],
            'total_order' => ['type' => Type::float()],
            'details' => ['type' => Type::listOf(GraphQL::type('OrderDetail'))],
        ];
    }
}