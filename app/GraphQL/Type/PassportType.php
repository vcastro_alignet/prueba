<?php

namespace Alobd\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class PassportType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Passport',
    ];

    public function fields()
    {
        return [
            'token_type' => ['type' => Type::string()],
            'expires_in' => ['type' => Type::int()],
            'access_token' => ['type' => Type::string()],
            'refresh_token' => ['type' => Type::string()],
        ];
    }
}