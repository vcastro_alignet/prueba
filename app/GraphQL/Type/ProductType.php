<?php

namespace Alobd\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class ProductType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Product',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'category_id' => ['type' => Type::string()],
            'distributor_id' => ['type' => Type::string()],
            'brand_name' => ['type' => Type::string()],
            'ean' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'description' => ['type' => Type::string()],
            'packing' => ['type' => Type::string()],
            'factor' => ['type' => Type::int()],
            'price' => ['type' => Type::float()],
            'code' => ['type' => Type::string()],
            'image' => ['type' => Type::string()],
            'gs1' => ['type' => Type::boolean()],
        ];
    }
}