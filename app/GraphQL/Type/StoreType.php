<?php

namespace Alobd\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class StoreType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Store',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'type_id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'ruc' => ['type' => Type::string()],
            'address1' => ['type' => Type::string()],
            'address2' => ['type' => Type::string()],
            'phone' => ['type' => Type::string()],
            'latitude' => ['type' => Type::string()],
            'longitude' => ['type' => Type::string()],
        ];
    }
}