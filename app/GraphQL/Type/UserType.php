<?php

namespace Alobd\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
    ];

    public function fields()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'email' => ['type' => Type::string()],
            'first_name' => ['type' => Type::string()],
            'last_name' => ['type' => Type::string()],
            'is_pin' => ['type' => Type::boolean()],
            'mobile' => ['type' => Type::string()],
            'dni' => ['type' => Type::string()],
            'promotional_code' => ['type' => Type::string()],
            'store' => ['type' => GraphQL::type('Store')],
        ];
    }
}