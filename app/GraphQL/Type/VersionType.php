<?php

namespace Alobd\GraphQL\Type;

use Folklore\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class VersionType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Version',
    ];

    public function fields()
    {
        return [
            'product_code' => ['type' => Type::string()],
            'version_code' => ['type' => Type::int()],
            'version_name' => ['type' => Type::string()],
            'required' => ['type' => Type::boolean()],
            'platform' => ['type' => Type::string()],
            'uploaded' => ['type' => Type::string()],
        ];
    }
}