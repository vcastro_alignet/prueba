<?php

namespace Alobd\Http\Controllers\Test;

use Illuminate\Http\Request;
use Alobd\Http\Controllers\Controller;
use Laravel\Passport\HasApiTokens;

class TestController extends Controller
{
    use HasApiTokens;

    public function __construct()
    {
        $this->middleware('auth')->only(['index']);
    }

    public function index()
    {
        return "hola";
    }

}
