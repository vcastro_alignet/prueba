<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public $incrementing = false;

    protected $primaryKey = 'id';
}
