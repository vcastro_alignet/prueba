<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'distributor_id', 'payment_id', 'payment_state', 'order_state', 'user_id'. 'total_paid', 'total_products',
        'total_discounts'
    ];

    public function distributor()
    {
        return $this->belongsTo(Distributor::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function payment_state()
    {
        return $this->belongsTo(PaymentState::class, 'payment_state_id');
    }

    public function order_state()
    {
        return $this->belongsTo(OrderState::class, 'order_state_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }
}
