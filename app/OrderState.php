<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class OrderState extends Model
{
    public $incrementing = false;

    protected $primaryKey = 'id';

    public function lang()
    {
        return $this->hasMany(OrderStateLang::class, 'order_state_id');
    }
}
