<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class OrderStateLang extends Model
{
    public $incrementing = false;

    protected $primaryKey = 'order_state_id';

    protected $table = 'order_state_lang';
}
