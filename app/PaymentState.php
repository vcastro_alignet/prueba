<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class PaymentState extends Model
{
    public $incrementing = false;

    protected $primaryKey = 'id';

    public function lang()
    {
        return $this->hasMany(PaymentStateLang::class, 'payment_state_id');
    }

}
