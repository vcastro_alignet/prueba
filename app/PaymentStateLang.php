<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class PaymentStateLang extends Model
{
    public $incrementing = false;

    protected $primaryKey = 'payment_state_id';

    protected $table = 'payment_state_lang';
}
