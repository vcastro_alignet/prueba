<?php

namespace Alobd;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $table = 'productsView';

    public function getBrandNameAttribute()
    {
        return $this->attributes['brand'];
    }
    public function getImageAttribute()
    {
        return 'http://prod.alobodega.com/ImageStreamer.ashx?requesttype=1&image='.$this->attributes['image'];
    }
}
