<?php

namespace Alobd;

use Alobd\Traits\Attributes\StoreAttributes;
use Alobd\Traits\GenUuid;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use StoreAttributes, GenUuid;

    protected $connection = 'sqlsrv_alobc';

    protected $table = 'Store';

    protected $primaryKey = 'Id';

    public $incrementing = false;


}
