<?php

namespace Alobd\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

trait ApiResponser
{
    protected function errorResponse($message, $code)
    {
        $structure = [
            'errors' => $message
        ];

        return response()->json($structure, $code);
    }
}