<?php

namespace Alobd\Traits\Attributes;

trait CategoryAttributes
{
    public function getIdAttribute()
    {
        return $this->attributes['Id'];
    }

    public function getDistributorIdAttribute()
    {
        return $this->attributes['DistributorId'];
    }
}