<?php

namespace Alobd\Traits\Attributes;

trait DistributorAttributes
{
    public function getIdAttribute()
    {
        return $this->attributes['Id'];
    }

    public function getNameAttribute()
    {
        return $this->attributes['Name'];
    }

    public function getCompanyAttribute()
    {
        return $this->attributes['Company'];
    }

    public function getRucAttribute()
    {
        return $this->attributes['Ruc'];
    }

    public function getAddressAttribute()
    {
        return $this->attributes['Address'];
    }

    public function getMinOrderAttribute()
    {
        return $this->attributes['MinOrder'];
    }

    public function getColorAttribute()
    {
        return $this->attributes['Color'];
    }

    public function getImageAttribute()
    {
        return "https://s3.amazonaws.com/easymarketapp/".$this->attributes['Image'];
    }

}