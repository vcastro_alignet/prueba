<?php

namespace Alobd\Traits\Attributes;

trait StoreAttributes
{
    public function getIdAttribute()
    {
        return $this->attributes['Id'];
    }

    public function getTypeIdAttribute()
    {
        return $this->attributes['TypeId'];
    }

    public function getNameAttribute()
    {
        return $this->attributes['Name'];
    }

    public function getRucAttribute()
    {
        return $this->attributes['RUC'];
    }

    public function getAddress1Attribute()
    {
        return $this->attributes['Address1'];
    }

    public function getAddress2Attribute()
    {
        return $this->attributes['Address2'];
    }

    public function getPhoneAttribute()
    {
        return $this->attributes['Phone'];
    }

    public function getLatitudeAttribute()
    {
        return $this->attributes['Latitude'];
    }

    public function getLongitudeAttribute()
    {
        return $this->attributes['Longitude'];
    }
}