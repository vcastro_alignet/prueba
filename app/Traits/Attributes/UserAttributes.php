<?php

namespace Alobd\Traits\Attributes;

trait UserAttributes
{
    public function getIdAttribute()
    {
        return $this->attributes['Id'];
    }

    public function getFirstNameAttribute()
    {
        return $this->attributes['FirstName'];
    }

    public function getLastNameAttribute()
    {
        return $this->attributes['LastName'];
    }

    public function getEmailAttribute()
    {
        return $this->attributes['Email'];
    }

    public function getMobileAttribute()
    {
        return $this->attributes['Mobile'];
    }

    public function getDniAttribute()
    {
        return $this->attributes['DNI'];
    }

    public function getPromotionalCodeAttribute()
    {
        return $this->attributes['PromotionalCode'];
    }
}