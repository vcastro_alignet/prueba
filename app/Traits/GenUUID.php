<?php

namespace Alobd\Traits;

use Webpatser\Uuid\Uuid;


trait GenUuid
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}