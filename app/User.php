<?php

namespace Alobd;

use Alobd\Traits\GenUuid;
use Alobd\Traits\Attributes\UserAttributes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use UserAttributes, Notifiable, HasApiTokens, GenUuid;

    protected $connection = 'sqlsrv_alobc';

    protected $table = 'Users';

    protected $primaryKey = 'Id';

    public $incrementing = false;

    public function findForPassport($username) {
        return $this->where('Mobile', $username)->first();
    }

    public function validateForPassportPasswordGrant($password)
    {
        $request = Request::all();
        $customer = User::where('Mobile', $request['username'])->where('Password', $password)->first();

        if ($customer)
            return true;
        else
            return Hash::check($password, $this->Password);
    }

    public function getIsPinAttribute()
    {
        $data = ['password' => $this->Password];
        $rules = ['password' => 'int|max:9999'];

        $v = \Validator::make($data, $rules);

        if ($v->valid())
            return true;
        return false;
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'StoreId');
    }
}
