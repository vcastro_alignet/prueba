<?php


return [

    /*
     * The prefix for routes
     */
    'prefix' => 'graphql',

    /*
     * The routes to make GraphQL request. Either a string that will apply
     * to both query and mutation or an array containing the key 'query' and/or
     * 'mutation' with the according Route
     *
     * Example:
     *
     * Same route for both query and mutation
     *
     * 'routes' => [
     *     'query' => 'query/{graphql_schema?}',
     *     'mutation' => 'mutation/{graphql_schema?}',
     *      mutation' => 'graphiql'
     * ]
     *
     * you can also disable routes by setting routes to null
     *
     * 'routes' => null,
     */
    'routes' => '{graphql_schema?}',

    /*
     * The controller to use in GraphQL requests. Either a string that will apply
     * to both query and mutation or an array containing the key 'query' and/or
     * 'mutation' with the according Controller and method
     *
     * Example:
     *
     * 'controllers' => [
     *     'query' => '\Folklore\GraphQL\GraphQLController@query',
     *     'mutation' => '\Folklore\GraphQL\GraphQLController@mutation'
     * ]
     */
    'controllers' => \Folklore\GraphQL\GraphQLController::class.'@query',

    /*
     * The name of the input variable that contain variables when you query the
     * endpoint. Most libraries use "variables", you can change it here in case you need it.
     * In previous versions, the default used to be "params"
     */
    'variables_input_name' => 'variables',

    /*
     * Any middleware for the 'graphql' route group
     */
    'middleware' => [],

    /**
     * Any middleware for a specific 'graphql' schema
     */
    'middleware_schema' => [
        'default' => ['client.credentials'],
        'password' => ['auth:api'],
    ],

    /*
     * Any headers that will be added to the response returned by the default controller
     */
    'headers' => [],

    /*
     * Any JSON encoding options when returning a response from the default controller
     * See http://php.net/manual/function.json-encode.php for the full list of options
     */
    'json_encoding_options' => 0,

    /*
     * Config for GraphiQL (see (https://github.com/graphql/graphiql).
     * To dissable GraphiQL, set this to null
     */
    'graphiql' => [
        'routes' => '/graphiql/{graphql_schema?}',
        'controller' => \Folklore\GraphQL\GraphQLController::class.'@graphiql',
        'middleware' => ['web', 'auth'],
        'view' => 'graphql::graphiql',
        'composer' => \Folklore\GraphQL\View\GraphiQLComposer::class,
    ],

    /*
     * The name of the default schema used when no arguments are provided
     * to GraphQL::schema() or when the route is used without the graphql_schema
     * parameter
     */
    'schema' => 'default',

    /*
     * The schemas for query and/or mutation. It expects an array to provide
     * both the 'query' fields and the 'mutation' fields. You can also
     * provide an GraphQL\Schema object directly.
     *
     * Example:
     *
     * 'schemas' => [
     *     'default' => new Schema($config)
     * ]
     *
     * or
     *
     * 'schemas' => [
     *     'default' => [
     *         'query' => [
     *              'users' => 'App\GraphQL\Query\UsersQuery'
     *          ],
     *          'mutation' => [
     *
     *          ]
     *     ]
     * ]
     */
    'schemas' => [
        'default' => [
            'query' => [
                \Alobd\GraphQL\Query\UserQuery::class,
                \Alobd\GraphQL\Query\VersionQuery::class,
            ],
            'mutation' => [
                \Alobd\GraphQL\Mutation\UserLoginMutation::class,
            ]
        ],
        'password' => [
            'query' => [
                \Alobd\GraphQL\Query\Password\DistributorsQuery::class,
                \Alobd\GraphQL\Query\Password\MeQuery::class,
                \Alobd\GraphQL\Query\Password\CategoriesQuery::class,
                \Alobd\GraphQL\Query\Password\ProductsQuery::class,
                \Alobd\GraphQL\Query\Password\ProductQuery::class,
                \Alobd\GraphQL\Query\Password\OrdersQuery::class,
                \Alobd\GraphQL\Query\Password\OrderStatesQuery::class,
                \Alobd\GraphQL\Query\Password\PaymentsQuery::class,
            ],
            'mutation' => [
                \Alobd\GraphQL\Mutation\Password\DeviceAddMutation::class,
                \Alobd\GraphQL\Mutation\Password\OrderAddMutation::class,
            ]
        ]
    ],

    /*
     * The types available in the application. You can access them from the
     * facade like this: GraphQL::type('user')
     *
     * Example:
     *
     * 'types' => [
     *     'user' => 'App\GraphQL\Type\UserType'
     * ]
     *
     * or without specifying a key (it will use the ->name property of your type)
     *
     * 'types' =>
     *     'App\GraphQL\Type\UserType'
     * ]
     */
    'types' => [
        \Alobd\GraphQL\Type\UserType::class,
        \Alobd\GraphQL\Type\PassportType::class,
        \Alobd\GraphQL\Type\DistributorType::class,
        \Alobd\GraphQL\Type\DeviceType::class,
        \Alobd\GraphQL\Type\StoreType::class,
        \Alobd\GraphQL\Type\CategoryType::class,
        \Alobd\GraphQL\Type\ProductType::class,
        \Alobd\GraphQL\Type\VersionType::class,
        \Alobd\GraphQL\Type\OrderType::class,
        \Alobd\GraphQL\Type\PaymentType::class,
        \Alobd\GraphQL\Type\PaymentStateType::class,
        \Alobd\GraphQL\Type\OrderStateType::class,
        \Alobd\GraphQL\Type\OrderDetailType::class
    ],

    /*
     * This callable will receive all the Exception objects that are caught by GraphQL.
     * The method should return an array representing the error.
     *
     * Typically:
     *
     * [
     *     'message' => '',
     *     'locations' => []
     * ]
     */
    'error_formatter' => [\Folklore\GraphQL\GraphQL::class, 'formatError'],

    /*
     * Options to limit the query complexity and depth. See the doc
     * @ https://github.com/webonyx/graphql-php#security
     * for details. Disabled by default.
     */
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null,
        'disable_introspection' => false
    ]
];
