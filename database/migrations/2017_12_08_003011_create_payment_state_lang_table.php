<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentStateLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_state_lang', function (Blueprint $table) {
            $table->string('payment_state_id', 5)->index();
            $table->string('lang_id', 2);
            $table->string('name', 100);

            $table->foreign('payment_state_id')->references('id')->on('payment_states')->onDelete('cascade');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_state_lang');
    }
}
