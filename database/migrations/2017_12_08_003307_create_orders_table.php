<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('distributor_id');
            $table->integer('payment_id');
            $table->string('payment_state_id', 5);
            $table->string('order_state_id', 5);
            $table->uuid('user_id');
            $table->uuid('store_id');
            $table->string('note', 350)->nullable();
            $table->double('total_products');
            $table->double('total_shipping');
            $table->double('total_discount');
            $table->double('total_order');
            $table->double('total_paid');
            $table->timestamps();

            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');
            $table->foreign('payment_state_id')->references('id')->on('payment_states')->onDelete('cascade');
            $table->foreign('order_state_id')->references('id')->on('order_states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
