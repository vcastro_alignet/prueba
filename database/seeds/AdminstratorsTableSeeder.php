<?php

use Illuminate\Database\Seeder;

class AdminstratorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Alobd\Administrator::insert([
            [
                'name' => 'Victor Castro',
                'email' => 'victor.castro@alignet.com',
                'password' => bcrypt('Sistemas25!'),
            ],
            [
                'name' => 'Jesus Briceño',
                'email' => 'jesus.briceno@easymarketapp.com',
                'password' => bcrypt('alobodega2017.'),
            ],
        ]);
    }
}
