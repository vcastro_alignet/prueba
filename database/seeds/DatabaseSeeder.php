<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AdminstratorsTableSeeder::class);
         $this->call(LanguagesTableSeeder::class);
         $this->call(OauthClientsTableSeeder::class);
         $this->call(PaymentsTableSeeder::class);
         $this->call(PaymentStateTableSeeder::class);
         $this->call(OrderStateTableSeeder::class);
         $this->call(VersionsTableSeeder::class);
    }
}
