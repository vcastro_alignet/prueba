<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Alobd\Language::insert([
            [
                'id' => 'es',
                'name' => 'Spanish'
            ],
            [
                'id' => 'en',
                'name' => 'English'
            ],
        ]);
    }
}
