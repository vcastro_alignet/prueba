<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        switch (env('APP_ENV')) {
            case 'local' :
                \DB::table('oauth_clients')->insert([
                    [
                        'user_id' => null,
                        'name' => 'Personal',
                        'secret' => 'HBthLMvWOKims7IwPVz7vX0gG0pNGtgu19Q9g377',
                        'redirect' => 'http://192.168.1.100:8082/auth/callback',
                        'personal_access_client' => 1,
                        'password_client' => 0,
                        'revoked' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                    [
                        'user_id' => null,
                        'name' => 'Password',
                        'secret' => 'hkOmrdGSRyzZMzgdPUh4KKNbtIkcz2WuMNiKtz8N',
                        'redirect' => 'http://192.168.1.100:8082/auth/callback',
                        'personal_access_client' => 0,
                        'password_client' => 1,
                        'revoked' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                    [
                        'user_id' => null,
                        'name' => 'Client Credentials',
                        'secret' => '8dwEpQP6LR5vivzmE4rXkzel6xaIgUOuhxxFD0jJ',
                        'redirect' => 'http://192.168.1.100:8082/auth/callback',
                        'personal_access_client' => 0,
                        'password_client' => 0,
                        'revoked' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                ]);
                break;

            case 'dev' :
                \DB::table('oauth_clients')->insert([
                    [
                        'user_id' => null,
                        'name' => 'Personal',
                        'secret' => 'HBthLMvWOKims7IwPVz7vX0gG0pNGtgu19Q9g377',
                        'redirect' => 'http://192.168.1.100:8082/auth/callback',
                        'personal_access_client' => 1,
                        'password_client' => 0,
                        'revoked' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                    [
                        'user_id' => null,
                        'name' => 'Password',
                        'secret' => 'qrKWKQuWXD9Ikozx5sr5GjF9WC2kqZG5bpF57zST',
                        'redirect' => 'http://dev.alobodega.com:8455/auth/callback',
                        'personal_access_client' => 0,
                        'password_client' => 1,
                        'revoked' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                    [
                        'user_id' => null,
                        'name' => 'Client Credentials',
                        'secret' => 'wVCBFTsp24qDO5UetGMFPE0l3OBA4odzJd4wsrRh',
                        'redirect' => 'http://dev.alobodega.com:8455/auth/callback',
                        'personal_access_client' => 0,
                        'password_client' => 0,
                        'revoked' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                ]);
                break;

            default:
                true;
        }
    }
}
