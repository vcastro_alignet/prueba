<?php

use Illuminate\Database\Seeder;

class OrderStateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Alobd\OrderState::insert([
            ['id' => 'O-000', 'send_notification' => false],
            ['id' => 'O-001', 'send_notification' => true],
            ['id' => 'O-010', 'send_notification' => true],
            ['id' => 'O-015', 'send_notification' => true],
            ['id' => 'O-025', 'send_notification' => true],
            ['id' => 'O-090', 'send_notification' => true],
            ['id' => 'O-999', 'send_notification' => false],
        ]);

        \Alobd\OrderStateLang::insert([
            [
                'order_state_id' => 'O-000',
                'lang_id' => 'es',
                'name' => 'Pedido Aceptado',
            ],
            [
                'order_state_id' => 'O-000',
                'lang_id' => 'en',
                'name' => 'Accept',
            ],
            [
                'order_state_id' => 'O-001',
                'lang_id' => 'es',
                'name' => 'Pedido Generado',
            ],
            [
                'order_state_id' => 'O-001',
                'lang_id' => 'en',
                'name' => 'Generated',
            ],
            [
                'order_state_id' => 'O-010',
                'lang_id' => 'es',
                'name' => 'Pedido en Proceso',
            ],
            [
                'order_state_id' => 'O-010',
                'lang_id' => 'en',
                'name' => 'In progress',
            ],
            [
                'order_state_id' => 'O-015',
                'lang_id' => 'es',
                'name' => 'Pedido Entregado',
            ],
            [
                'order_state_id' => 'O-015',
                'lang_id' => 'en',
                'name' => 'Delivered',
            ],
            [
                'order_state_id' => 'O-025',
                'lang_id' => 'es',
                'name' => 'Pedido Rechazado',
            ],
            [
                'order_state_id' => 'O-025',
                'lang_id' => 'en',
                'name' => 'Refused',
            ],
            [
                'order_state_id' => 'O-090',
                'lang_id' => 'es',
                'name' => 'Pedido Cancelado',
            ],
            [
                'order_state_id' => 'O-090',
                'lang_id' => 'en',
                'name' => 'Cancelled',
            ],
            [
                'order_state_id' => 'O-999',
                'lang_id' => 'es',
                'name' => 'Error Desconocido',
            ],
            [
                'order_state_id' => 'O-999',
                'lang_id' => 'en',
                'name' => 'Error unknown',
            ],
        ]);
    }
}
