<?php

use Illuminate\Database\Seeder;

class PaymentStateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Alobd\PaymentState::insert([
            ['id' => 'P-000', 'send_notification' => true],
            ['id' => 'P-001', 'send_notification' => false],
            ['id' => 'P-023', 'send_notification' => false],
            ['id' => 'P-025', 'send_notification' => true],
            ['id' => 'P-026', 'send_notification' => true],
            ['id' => 'P-090', 'send_notification' => true],
            ['id' => 'P-091', 'send_notification' => false],
        ]);

        \Alobd\PaymentStateLang::insert([
            [
                'payment_state_id' => 'P-000',
                'lang_id' => 'es',
                'name' => 'Aceptado',
            ],
            [
                'payment_state_id' => 'P-000',
                'lang_id' => 'en',
                'name' => 'Accept',
            ],
            [
                'payment_state_id' => 'P-001',
                'lang_id' => 'es',
                'name' => 'Pendiente',
            ],
            [
                'payment_state_id' => 'P-001',
                'lang_id' => 'en',
                'name' => 'Pending',
            ],
            [
                'payment_state_id' => 'P-023',
                'lang_id' => 'es',
                'name' => 'Tarjeta inválida',
            ],
            [
                'payment_state_id' => 'P-023',
                'lang_id' => 'en',
                'name' => 'Invalid card',
            ],
            [
                'payment_state_id' => 'P-025',
                'lang_id' => 'es',
                'name' => 'Rechazado',
            ],
            [
                'payment_state_id' => 'P-025',
                'lang_id' => 'en',
                'name' => 'Rejected',
            ],
            [
                'payment_state_id' => 'P-026',
                'lang_id' => 'es',
                'name' => 'Anulado',
            ],
            [
                'payment_state_id' => 'P-026',
                'lang_id' => 'en',
                'name' => 'Annulled,',
            ],
            [
                'payment_state_id' => 'P-090',
                'lang_id' => 'es',
                'name' => 'Cancelado',
            ],
            [
                'payment_state_id' => 'P-090',
                'lang_id' => 'en',
                'name' => 'Canceled',
            ],
            [
                'payment_state_id' => 'P-091',
                'lang_id' => 'es',
                'name' => 'Tiempo de espera agotado',
            ],
            [
                'payment_state_id' => 'P-091',
                'lang_id' => 'en',
                'name' => 'Timeout',
            ],
        ]);
    }
}
