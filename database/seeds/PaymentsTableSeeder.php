<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Alobd\Payment::insert([
            ['name' => 'payme'],
            ['name' => 'contraentrega'] ,
        ]);
    }
}
