<?php

use Illuminate\Database\Seeder;

class VersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Alobd\Version::insert([
            [
                'product_code' => 'ALOBD',
                'version_code' => '16',
                'version_name' => 'AloBodeguero',
                'required' => true,
                'platform' => 'android',
                'uploaded' => \Carbon\Carbon::now(),
            ],

        ]);
    }
}
