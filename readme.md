
#### Cambiar el format por default
https://blogs.msdn.microsoft.com/sqlserverfaq/2009/11/13/how-to-change-date-format-after-installing-sql-server/
```
USE [master]
GO

EXEC sys.sp_configure N'default language', N'0'
GO

RECONFIGURE WITH OVERRIDE
GO

ALTER LOGIN [myuser] WITH DEFAULT_LANGUAGE=[us_english]
GO
```

#### StoredProcedure Distributors Available
```
-- Description:	Get distributors available from location
-- =======================================================
CREATE PROCEDURE dbo.GetDistributorsAvailable(
	@StoreId AS UNIQUEIDENTIFIER
)
AS
	SELECT *
	FROM Distributor_LOCAL.dbo.Distributors 
	WHERE Active = 1
GO
```

#### Tablas Vistas de Categorias
```
CREATE VIEW categoriesView AS
    SELECT
        pd.CategoryId AS id,
        pd.DistributorId AS distributor_id
    FROM Distribuidores_DEV.dbo.Products pd
        INNER JOIN AloBodega_DEV.dbo.Products pa ON pd.EAN = pa.UPC
    GROUP BY CategoryId, DistributorId
```

#### Tablas Vistas de Productos
```
CREATE VIEW productsView AS
    SELECT 
        pd.Id as id,
        pd.CategoryId as category_id,
        pd.EAN as ean,
        pd.DistributorId as distributor_id,
        pd.Brand as brand,
        pd.Name as name,
        pd.Description as description,
        pd.Embalaje AS packing,
        pd.Factor as factor,
        round(pd.Precio * pd.Comision, 2) as price,
        pd.Code as code,
        pa.Image as image,
        pa.GS1 as gs1
    FROM Distribuidores_DEV.dbo.Products pd
        INNER JOIN AloBodega_DEV.dbo.Products pa ON pd.EAN = pa.UPC
    WHERE pd.Online = 1
```


###### Dummy Order Table
```
INSERT INTO orders (
  distributor_id, payment_id, payment_state_id, order_state_id, user_id,
  store_id, total_products, total_shipping, total_discount, total_order,
  total_paid
) VALUES (
  'F28345CB-5058-4294-B3FD-785E15789395', 1, 'P-000', 'O-001', 'D36CE5D5-26AE-468A-9FF6-0019F0FE7131',
  '55A9DDC0-C975-4B8D-BB4A-3A80B4514964', 22, 0, 0.50, 21.50,
  21.50
)


INSERT INTO order_details (id, order_id, product_id, quantity, added_at)
VALUES
  (newid(), 1, 'DCE0A3C5-3EDC-4BAD-9A69-50C19587554C', 4, '2017-12-13 10:10:00'),
  (newid(), 1, '1FD47DD1-014B-4D9F-9510-4E6115C97F0A', 1, '2017-12-13 10:14:00')
  ```