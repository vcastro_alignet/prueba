@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Servicios</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>
                        <li><a href="{{ route('graphql.graphiql') }}" target="_blank">GraphiQL -  Client Credentials</a></li>
                        <li><a href="{{ route('graphql.graphiql', 'password') }}" target="_blank">GraphiQL -  Password</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4 ">
            <div class="panel panel-default">
                <div class="panel-heading">Administradores <a class="pull-right" href="{{ route('register') }}"> Añadir </a></div>

                <div class="panel-body">

                    <ul>
                        @forelse ($admins as $admin)
                            <li>{{ $admin->name }} <br> <small>{{ $admin->email }} </small></li>
                            <br>
                        @empty
                            <li>No hay usuarios registrados.</li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
