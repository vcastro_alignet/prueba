<!DOCTYPE html>
<html>
    <head>
        <title>GraphiQL</title>
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
                width: 100%;
                overflow: hidden;
            }
            #graphiql {
                height: 100vh;
            }
            .wrapper-token {
                background: linear-gradient(#f7f7f7, #e2e2e2);
                display: inline-block;
                border-bottom: 1px solid #d0d0d0;
                font-family: 'San Francisco', arial, sans-serif;
                padding: 7px 14px 6px;
                font-size: 14px;
                width: 100%;
            }

            .wrapper-token input {
                display: inline-block;
                width: 80%;
                padding: 5px;
                border: 0px;
                margin-left: 5px;
                font-size: 12px;
                color: #777777;
                border-radius: 3px;
            }

            .wrapper-token button#save-token{
                background: linear-gradient(#5cb85c,#449d44);
                border-radius: 3px;
                box-shadow: inset 0 0 0 1px rgba(0,0,0,.2), 0 1px 0 rgba(255,255,255,.7), inset 0 1px #fff;
                color: #ffffff;
                border: 0px;
                margin: 0 5px;
                padding: 3px 11px 5px;
            }

            .wrapper-token button#remove-token{
                background: linear-gradient(#d9534f, #c9302c);
                border-radius: 3px;
                box-shadow: inset 0 0 0 1px rgba(0,0,0,.2), 0 1px 0 rgba(255,255,255,.7), inset 0 1px #fff;
                color: white;
                border: 0px;
                margin: 0 5px;
                padding: 3px 11px 5px;
            }

            .graphiql-container .variable-editor {
                min-height: 150px!important;
            }

        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.10.2/graphiql.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.3/fetch.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.5.4/react.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.5.4/react-dom.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.11.3/graphiql.min.js"></script>
    </head>
    <body>
        <div class="wrapper-token" >
            <label>Token</label>
            <input id="jwt-token" placeholder="Paste token (without Bearer)">
            <button id="save-token" >✔</button>
            <button id="remove-token">✖</button>
        </div>
        <div id="graphiql">Loading...</div>
        <script>
            /**
             * This GraphiQL example illustrates how to use some of GraphiQL's props
             * in order to enable reading and updating the URL parameters, making
             * link sharing of queries a little bit easier.
             *
             * This is only one example of this kind of feature, GraphiQL exposes
             * various React params to enable interesting integrations.
             */

            // Parse the search string to get url parameters.
            var search = window.location.search;
            var parameters = {};
            var save_token = document.getElementById('save-token');
            var remove_token = document.getElementById('remove-token');
            var app_env = window.location.hostname+':'+location.port;

            document.getElementById('jwt-token').value = localStorage.getItem('graphiql:passport_'+app_env);

            save_token.onclick = function () {
                localStorage.setItem('graphiql:passport_'+app_env, document.getElementById('jwt-token').value);
                location.reload();
            }

            remove_token.onclick = function(){
                localStorage.removeItem('graphiql:passport_'+app_env);
                location.reload();
            }

            search.substr(1).split('&').forEach(function (entry) {
                var eq = entry.indexOf('=');
                if (eq >= 0) {
                    parameters[decodeURIComponent(entry.slice(0, eq))] =
                        decodeURIComponent(entry.slice(eq + 1));
                }
            });

            // if variables was provided, try to format it.
            if (parameters.variables) {
                try {
                    parameters.variables =
                        JSON.stringify(JSON.parse(parameters.variables), null, 2);
                } catch (e) {
                    // Do nothing, we want to display the invalid JSON as a string, rather
                    // than present an error.
                }
            }

            // When the query and variables string is edited, update the URL bar so
            // that it can be easily shared
            function onEditQuery(newQuery) {
                parameters.query = newQuery;
                updateURL();
            }

            function onEditVariables(newVariables) {
                parameters.variables = newVariables;
                updateURL();
            }

            function onEditOperationName(newOperationName) {
                parameters.operationName = newOperationName;
                updateURL();
            }

            function updateURL() {
                var newSearch = '?' + Object.keys(parameters).filter(function (key) {
                    return Boolean(parameters[key]);
                }).map(function (key) {
                    return encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]);
                }).join('&');
                history.replaceState(null, null, newSearch);
            }

            // Defines a GraphQL fetcher using the fetch API.
            function graphQLFetcher(graphQLParams) {
                const jwtToken = document.getElementById('jwt-token').value;
                if(jwtToken) localStorage.setItem('graphiql:passport_'+app_env, jwtToken);

                return fetch('<?php echo $graphqlPath; ?>', {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': jwtToken ? 'Bearer '+jwtToken : ''
                    },
                    body: JSON.stringify(graphQLParams),
                    credentials: 'include',
                }).then(function (response) {
                    return response.text();
                }).then(function (responseBody) {
                    try {
                        return JSON.parse(responseBody);
                    } catch (error) {
                        return responseBody;
                    }
                });
            }

            // Render <GraphiQL /> into the body.
            ReactDOM.render(
                React.createElement(GraphiQL, {
                    fetcher: graphQLFetcher,
                    query: parameters.query,
                    variables: parameters.variables,
                    operationName: parameters.operationName,
                    onEditQuery: onEditQuery,
                    onEditVariables: onEditVariables,
                    onEditOperationName: onEditOperationName
                }),
                document.getElementById('graphiql')
            );
        </script>
    </body>
</html>
