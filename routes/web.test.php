<?php

use Carbon\Carbon;

Route::resource('test', 'Test\TestController', ['only' => ['index']]);

Route::get('t1', function(){
    return \Alobd\Order::first()->orderState->lang;
        //->lang->where('code_lang', 'en')->first();
});

Route::get('t2', function(){
    return \Alobd\PaymentState::first()->lang;
});