<?php

namespace Tests\Feature;

use Alobd\Distributor;
use Tests\TestCase;
use Laravel\Passport\Client as Passport;
use GuzzleHttp\Client as HttpRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Client as PassportClient;

class GraphqlCredentialsTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $http = new HttpRequest;

        $passport = Passport::where('password_client', false)
            ->where('personal_access_client', false)
            ->first();

        $response = $http->post(route('oauth.token'), [
            'form_params' => [
                'grant_type'    => 'client_credentials',
                'client_id'     => $passport->id,
                'client_secret' => $passport->secret,
            ]
        ]);

        $this->defaultHeaders = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.json_decode($response->getBody(), false)->access_token,
        ];

    }


    public function testQueryUser() : void
    {
        $q = json_decode('
            {
                "query": "query getUser($m: String!, $d: String!){ user (mobile: $m dni: $d)  { id email first_name last_name mobile}}",
                "variables": {
                    "m": "+51 961509467",
                    "d": "45454449-3"
                },
                "operationName": "getUser"
            }
        ', true);

        $res = $this->postJson(route('graphql.query.post'), $q);

        $res->assertStatus(200)
            ->assertJson([
                'data' => [
                    'user' => [
                        'mobile' => '+51 961509467'
                    ]
                ],
            ]);
    }

    public function testQueryVersion() : void
    {
        $q = json_decode('
            {
                "query": "query getVersions($prod: String!, $plat: String!) { version(product_code:$prod  platform:$plat) { product_code  version_code  version_name  required  platform  uploaded } }",
                "variables": {
                    "prod":"alobd",
                    "plat": "android"
                },
                "operationName":"getVersions"
                
            }
        ', true);

        $res = $this->postJson(route('graphql.query.post'), $q);

        $res->assertStatus(200)
            ->assertJson([
                'data' => [
                    'version' => [
                        'product_code' => 'ALOBD',
                        'platform' => 'android'
                    ]
                ],
            ]);
    }
}
