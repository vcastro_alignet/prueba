<?php

namespace Tests\Feature;

use Alobd\Distributor;
use Tests\TestCase;
use Laravel\Passport\Client as Passport;
use GuzzleHttp\Client as HttpRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Client as PassportClient;

class GraphqlPasswordTest extends TestCase
{
    private $mobile = '+51 961509467';

    private $password = '2244';

    public function setUp()
    {
        parent::setUp();

        $http = new HttpRequest;

        $passport = Passport::where('password_client', true)
            ->where('personal_access_client', false)
            ->first();

        $response = $http->post(route('oauth.token'), [
            'form_params' => [
                'grant_type'    => 'password',
                'client_id'     => $passport->id,
                'client_secret' => $passport->secret,
                'username'      => $this->mobile,
                'password'      => $this->password,
                'scope'         => null,
            ]
        ]);

        $this->defaultHeaders = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.json_decode($response->getBody(), false)->access_token,
        ];

    }


    public function testQueryMe() : void
    {
        $queryGetMe = json_decode('
            {
                "query":"query getMe{ me { id email first_name last_name mobile dni store {id name ruc address1 address2 latitude longitude}}}",
                "variables":null,
                "operationName":"getMe"
            }
        ', true);

        $res = $this->postJson(route('graphql.query.post', ['password']), $queryGetMe);

        $res->assertStatus(200)
            ->assertJson([
                'data' => [
                    'me' => [
                        'mobile' => $this->mobile
                    ]
                ],
            ]);
    }

    public function testQueryDistributors() : void
    {
        $queryGetMe = json_decode('
            {
                "query": "query getDistributors{ distributors{ id name company address image}}"
            }
        ', true);

        $res = $this->postJson(route('graphql.query.post', ['password']), $queryGetMe);

        $dis = Distributor::first();

        $res->assertStatus(200)
            ->assertJson([
                'data' => [
                    'distributors' => [
                        [
                            'id' => $dis->id,
                        ]
                    ]
                ],
            ]);
    }
}
